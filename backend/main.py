# main.py

import falcon
from wsgiref import simple_server

# Falcon follows the REST architectural style, meaning (among
# other backend) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
class MyBackend:
    def on_get(self, req, resp):
        """Handles GET requests"""
        print '----> backend enter'
        resp.status = falcon.HTTP_200  # This is the default status

        f = open('../animation/index.html', 'r')
        resp.body = ''.join(f.readlines())
        resp.content_type = 'text/html'

# falcon.API instances are callable WSGI apps
wsgi_app = app = falcon.API()

# Resources are represented by long-lived class instances
backend = MyBackend()

# backend will handle all requests to the '/backend' URL path
app.add_route('/api', backend)

print '----> finish loading'

if __name__ == '__main__':
    httpd = simple_server.make_server('0.0.0.0', 8000, app)
    httpd.serve_forever()
